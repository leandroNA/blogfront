import { createRouter, createWebHistory } from 'vue-router'
import ListPost from '../views/post/ListPost.vue'
import indexpost from '../views/post/IndexPost.vue'
import CreatePost from '../views/post/CreatePost.vue'
import EditPost from '../views/post/EditPost.vue'
import IndexAuthor from '../views/author/IndexAuthor.vue'
import CreateAuthor from '../views/author/CreateAuthor.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'listpost',
      component: ListPost
    },
    {
      path: '/admin/post',
      name: 'indexpost',
      component: indexpost
    },
    {
      path: '/admin/crear',
      name: 'createpost',
      component: CreatePost
    },
    {
      path: '/admin/post/:id',
      name: 'editpost',
      component: EditPost

    },
    {
      path: '/admin/author',
      name: 'indexauthor',
      component: IndexAuthor
    },
    {
      path: '/admin/author/crear',
      name: 'createauthor',
      component: CreateAuthor
    },
  
  ]
})

export default router
